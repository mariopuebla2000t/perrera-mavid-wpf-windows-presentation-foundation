# Prototipo de interfaz gráfica de usuario - WPF Windows Presentation Foundation
Proyecto desarrollado entre 2 alumnos como parte de la asignatura **[42320	- Interacción Persona Ordenador I](https://guiae.uclm.es/vistaGuia/407/42320)** en el tercer curso de ingeniería informática. En este trabajo, se ha creado un prototipo de interfaz gráfica de usuario (prototipo de aplicación de escritorio) para la gestión de una perrera usando WPF. 

## Tecnologías Utilizadas
**Lenguaje de programación**: C# y XML(datos de prueba)  
**Herramientas de desarrollo**: Visual Studio 2019  

Más información en los PDF's asociados.
