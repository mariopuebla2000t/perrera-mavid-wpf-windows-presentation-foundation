﻿using System;
using System.Drawing;
using System.Windows.Media.Imaging;

public class Socio
{
	public string NombreCompleto { get; set; }
	public string DNI { get; set; }
	public string Telefono { get; set; }
	public string Email { get; set; }
	public int CuantiaMensual { get; set; }

	public Socio(string NombreCompleto, string DNI, string Telefono, string Email, int CuantiaMensual)
    {
		this.NombreCompleto = NombreCompleto;
		this.DNI = DNI;	
		this.Telefono = Telefono;	
		this.Email = Email;
		this.CuantiaMensual = CuantiaMensual;
    }

}
