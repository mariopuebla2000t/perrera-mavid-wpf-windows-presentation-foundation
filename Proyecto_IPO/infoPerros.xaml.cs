﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;

namespace Proyecto_IPO
{
    /// <summary>
    /// Lógica de interacción para infoPerros.xaml
    /// </summary>
    public partial class infoPerros : Window
    {
        listadoPerros listaPerros = new listadoPerros(); 
        public infoPerros(listadoPerros listaPerros)
        {
            this.listaPerros = listaPerros;
            InitializeComponent();
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "Formatos soportados|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                string dest = System.IO.Path.Combine(Environment.CurrentDirectory, "Perros", System.IO.Path.GetFileName(op.FileName));
                string perroPath = System.IO.Path.Combine("Perros", System.IO.Path.GetFileName(op.FileName));
                txtPath.Text = op.FileName;
                try
                {
                    File.Copy(op.FileName, dest);
                    imgPerro.Source = new BitmapImage(new Uri(dest));
                }
                catch
                {
                    MessageBox.Show("Error, una imagen con el mismo nombre ya está almacenada en la carpeta.","Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    txtPath.Text = "";
                }


            }

            txtNombre.IsEnabled = true;
            txtSexo.IsEnabled = true;
            txtRaza.IsEnabled = true;
            txtEdad.IsEnabled = true;
            txtFecha.IsEnabled = true;
            txtDescripcion.IsEnabled = true;
            txtDNIApadrinador.IsEnabled = true;
            btnAddPerro.IsEnabled = true;   
        }

        private void btnAddPerro_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(txtNombre.Text) || String.IsNullOrEmpty(txtSexo.Text) ||
                String.IsNullOrEmpty(txtEdad.Text) || String.IsNullOrEmpty(txtFecha.Text) ||
                String.IsNullOrEmpty(txtRaza.Text) || String.IsNullOrEmpty(txtDescripcion.Text))
            {
                MessageBox.Show("Por favor, asegurese de que haya rellenado todos los campos correctamente.","Rellene todos los campos", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {

                string Path = Environment.CurrentDirectory.ToString();
                string Parent = Directory.GetParent(Directory.GetParent(Path).FullName).FullName;

                XmlDocument doc = listaPerros.doc;
                XmlNode root = doc.DocumentElement;
                //Create a new attrtibute.  
                XmlElement elem = doc.CreateElement("Perro");
                XmlAttribute nombre = doc.CreateAttribute("Nombre");
                XmlAttribute sexo = doc.CreateAttribute("Sexo");
                XmlAttribute raza = doc.CreateAttribute("Raza");
                XmlAttribute edad = doc.CreateAttribute("Edad");
                XmlAttribute fecha = doc.CreateAttribute("Fecha");
                XmlAttribute descripcion = doc.CreateAttribute("Descripcion");
                XmlAttribute foto = doc.CreateAttribute("Foto");
                XmlAttribute DNIApadrinador = doc.CreateAttribute("DNIApadrinador");

                if (!btnAddPerro.Content.Equals("AÑADIR PERRO AL LISTADO"))
                {
                    Perro perro = listaPerros.lstPerros.SelectedItem as Perro;
                    listaPerros.listPerros.RemoveAt(listaPerros.lstPerros.SelectedIndex);
                    listaPerros.lstPerros.Items.Refresh();

                    XmlNode node = doc.SelectSingleNode("/Perros/Perro[@Nombre='" + perro.Nombre + "']");
                    XmlNode parent = node.ParentNode;
                    parent.RemoveChild(node);
                    string newXML = doc.OuterXml;
                }

                nombre.Value = txtNombre.Text;
                sexo.Value = txtSexo.Text;
                raza.Value = txtRaza.Text;  
                edad.Value = txtEdad.Text;
                fecha.Value = txtFecha.Text;    
                descripcion.Value = txtDescripcion.Text;
                foto.Value = "/Perros/" + System.IO.Path.GetFileName(txtPath.Text);

                if(txtDNIApadrinador.Text == null)
                {
                    DNIApadrinador.Value = "0";
                }
                else
                {
                    DNIApadrinador.Value = txtDNIApadrinador.Text;
                }

                elem.Attributes.Append(nombre);
                elem.Attributes.Append(sexo);
                elem.Attributes.Append(raza);
                elem.Attributes.Append(edad);
                elem.Attributes.Append(fecha);
                elem.Attributes.Append(descripcion);
                elem.Attributes.Append(foto);
                elem.Attributes.Append(DNIApadrinador);

                root.InsertAfter(elem, root.LastChild);
                doc.Save(System.IO.Path.Combine(Parent, "XMLs/Perros.xml"));

                if(btnAddPerro.Content.Equals("AÑADIR PERRO AL LISTADO"))
                {
                    MessageBox.Show("El perro con nombre " + txtNombre.Text + " ha sido añadido con éxito a la lista y al fichero XML.", "Perro añadido", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("El perro con nombre " + txtNombre.Text + " ha sido editado con éxito", "Perro editado", MessageBoxButton.OK, MessageBoxImage.Information);
                }

                listaPerros.listPerros.Add(new Perro(txtNombre.Text, txtSexo.Text, txtRaza.Text, Convert.ToInt32(txtEdad.Text), txtFecha.Text,
                    txtDescripcion.Text, new Uri(Environment.CurrentDirectory + foto.Value), txtDNIApadrinador.Text));
                listaPerros.lstPerros.Items.Refresh();

                this.Close();
            }
        }

        private void btnVolverAtras_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnApadrinado_Click(object sender, RoutedEventArgs e)
        {
            Socios socios = new Socios();
            bool encontrado = false;
            Perro perro = listaPerros.lstPerros.SelectedItem as Perro;
            foreach (Socio item in socios.listSocios)
            {
                if (item.DNI.Equals(perro.DNIApadrinador))
                {
                    infoApadrinador apadrinador = new infoApadrinador();
                    apadrinador.txtNombreSocios.Text = item.NombreCompleto;
                    apadrinador.txtDNISocios.Text = item.DNI;
                    apadrinador.txtTlfSocios.Text = item.Telefono;
                    apadrinador.txtEmailSocios.Text = item.Email;
                    apadrinador.txtCuantiaMensual.Text = Convert.ToString(item.CuantiaMensual);
                    apadrinador.Show();
                    socios.Close();
                    encontrado = true;
                }
            }

            if (!encontrado)
            {
                MessageBox.Show("Error, el DNI del apadrinador establecido no se encuentra en la lista de socios.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                socios.Close();
            }

        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
