﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;

namespace Proyecto_IPO
{
    /// <summary>
    /// Lógica de interacción para Socios.xaml
    /// </summary>
    public partial class Socios : Window
    {
        public List<Socio> listSocios;
        public XmlDocument doc = new XmlDocument();

        public Socios()
        {
            InitializeComponent();
            listSocios = CargarXMLSocios();
            lstSocios.ItemsSource = listSocios;
        }

        private List<Socio> CargarXMLSocios()
        {
            List<Socio> listado = new List<Socio>();
            // Cargar contenido de prueba

            var fichero = Application.GetResourceStream(new Uri("XMLs/Socios.xml", UriKind.Relative));
            doc.Load(fichero.Stream);
            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                var nuevoSocio = new Socio("", "", "", "", 0);
                nuevoSocio.NombreCompleto = node.Attributes["NombreCompleto"].Value;
                nuevoSocio.DNI = node.Attributes["DNI"].Value;
                nuevoSocio.Telefono = node.Attributes["Telefono"].Value;
                nuevoSocio.Email = node.Attributes["Email"].Value;
                nuevoSocio.CuantiaMensual = Convert.ToInt32(node.Attributes["CuantiaMensual"].Value);
                listado.Add(nuevoSocio);
            }
            return listado;
        }

        private void btnElimSocios_Click(object sender, RoutedEventArgs e)
        {
            Socio socio = lstSocios.SelectedItem as Socio;
            if (socio != null)
            {
                string message = "Está seguro de que quiere eliminar de la lista a " + socio.NombreCompleto;
                MessageBoxResult result = MessageBox.Show(message, "¿Eliminar de la lista?", MessageBoxButton.YesNo, MessageBoxImage.Information);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        listSocios.RemoveAt(lstSocios.SelectedIndex);
                        lstSocios.Items.Refresh();

                        XmlNode node = doc.SelectSingleNode("/Socios/Socio[@DNI='" + socio.DNI + "']");
                        XmlNode parent = node.ParentNode;
                        parent.RemoveChild(node);
                        string newXML = doc.OuterXml;
                        string Path = Environment.CurrentDirectory.ToString();
                        string Parent = Directory.GetParent(Directory.GetParent(Path).FullName).FullName;
                        doc.Save(System.IO.Path.Combine(Parent, "XMLs/Socios.xml"));
                        break;

                    case MessageBoxResult.No:
                        break;
                }
            }
            else
            {
                MessageBox.Show("Por favor, seleccione un sujeto.", "Seleccione un sujeto", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        private void btnAnadirSocios_Click(object sender, RoutedEventArgs e)
        {
            txtNombreSocios.IsEnabled = true;
            txtDNISocios.IsEnabled = true;  
            txtTlfSocios.IsEnabled = true;
            txtEmailSocios.IsEnabled = true;
            txtCuantiaMensual.IsEnabled = true;
            btnAnadirSocios.IsEnabled = false;

            txtNombreSocios.IsReadOnly = false;
            txtDNISocios.IsReadOnly = false;
            txtTlfSocios.IsReadOnly = false;
            txtEmailSocios.IsReadOnly = false;
            txtCuantiaMensual.IsReadOnly = false;

            txtNombreSocios.Text = "";
            txtDNISocios.Text = "";
            txtTlfSocios.Text = "";
            txtEmailSocios.Text = "";
            txtCuantiaMensual.Text = "";

            btnAceptarAdd.Content = "AÑADIR SOCIO";
            btnAceptarAdd.Visibility = Visibility.Visible;
        }

        private void btnEditarSocios_Click(object sender, RoutedEventArgs e)
        {
            if (lstSocios.SelectedItem != null)
            {
                Socio socio = (Socio)lstSocios.SelectedItem;
                txtNombreSocios.Text = socio.NombreCompleto;
                txtDNISocios.Text = socio.DNI;
                txtTlfSocios.Text = socio.Telefono;
                txtEmailSocios.Text = socio.Email;
                txtCuantiaMensual.Text = Convert.ToString(socio.CuantiaMensual);
                btnAceptarAdd.Visibility = Visibility.Hidden;

                txtNombreSocios.IsEnabled = true;
                txtDNISocios.IsEnabled = true;
                txtTlfSocios.IsEnabled = true;
                txtEmailSocios.IsEnabled = true;
                txtCuantiaMensual.IsEnabled = true;
                btnAnadirSocios.IsEnabled = false;

                txtNombreSocios.IsReadOnly = false;
                txtDNISocios.IsReadOnly = false;
                txtTlfSocios.IsReadOnly = false;
                txtEmailSocios.IsReadOnly = false;
                txtCuantiaMensual.IsReadOnly = false;

                btnAceptarAdd.Content = "EDITAR SOCIO";
                btnAceptarAdd.Visibility = Visibility.Visible;
            }
            else
            {
                MessageBox.Show("Por favor, seleccione un sujeto.", "Seleccione un sujeto", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }



        private void btnAceptarAdd_Edit_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(txtNombreSocios.Text) || String.IsNullOrEmpty(txtDNISocios.Text) ||
               String.IsNullOrEmpty(txtTlfSocios.Text) || String.IsNullOrEmpty(txtEmailSocios.Text) ||
               String.IsNullOrEmpty(txtCuantiaMensual.Text))
            {
                MessageBox.Show("Por favor, asegurese de que haya rellenado todos los campos correctamente.", "Rellene todos los campos", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                
                string Path = Environment.CurrentDirectory.ToString();
                string Parent = Directory.GetParent(Directory.GetParent(Path).FullName).FullName;

                XmlNode root = doc.DocumentElement;
                //Create a new attrtibute.  
                XmlElement elem = doc.CreateElement("Socio");
                XmlAttribute nombreCompleto = doc.CreateAttribute("NombreCompleto");
                XmlAttribute dni = doc.CreateAttribute("DNI");
                XmlAttribute telefono = doc.CreateAttribute("Telefono");
                XmlAttribute email = doc.CreateAttribute("Email");
                XmlAttribute cuantiaMensual = doc.CreateAttribute("CuantiaMensual");

                if (!btnAceptarAdd.Content.Equals("AÑADIR SOCIO"))
                {
                    Socio socio = lstSocios.SelectedItem as Socio;
                    listSocios.RemoveAt(lstSocios.SelectedIndex);
                    lstSocios.Items.Refresh();

                    XmlNode node = doc.SelectSingleNode("/Socios/Socio[@DNI='" + socio.DNI + "']");
                    XmlNode parent = node.ParentNode;
                    parent.RemoveChild(node);
                    string newXML = doc.OuterXml;
                }

                nombreCompleto.Value = txtNombreSocios.Text;
                dni.Value = txtDNISocios.Text;
                telefono.Value = txtTlfSocios.Text;
                email.Value = txtEmailSocios.Text;
                cuantiaMensual.Value = txtCuantiaMensual.Text;

                elem.Attributes.Append(nombreCompleto);
                elem.Attributes.Append(dni);
                elem.Attributes.Append(telefono);
                elem.Attributes.Append(email);
                elem.Attributes.Append(cuantiaMensual);

                root.InsertAfter(elem, root.LastChild);
                doc.Save(System.IO.Path.Combine(Parent, "XMLs/Socios.xml"));

                if (btnAceptarAdd.Content.Equals("AÑADIR SOCIO"))
                {
                    MessageBox.Show("El socio con nombre " + txtNombreSocios.Text + " ha sido añadido con éxito a la lista y al fichero XML.", "Socio añadido", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("El socio con nombre " + txtNombreSocios.Text + " ha sido editado con éxito", "Socio editado", MessageBoxButton.OK, MessageBoxImage.Information);
                }

                txtNombreSocios.IsEnabled = false;
                txtDNISocios.IsEnabled = false;
                txtTlfSocios.IsEnabled = false;
                txtEmailSocios.IsEnabled = false;
                txtCuantiaMensual.IsEnabled = false;
                btnAnadirSocios.IsEnabled = true;

                listSocios.Add(new Socio(txtNombreSocios.Text, txtDNISocios.Text,
                    txtTlfSocios.Text, txtEmailSocios.Text, Convert.ToInt32(txtCuantiaMensual.Text)));
                lstSocios.Items.Refresh();

            }
        }


        private void lstSocios_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lstSocios.SelectedItem != null)
            {
                Socio socio = (Socio)lstSocios.SelectedItem;
                txtNombreSocios.Text = socio.NombreCompleto;
                txtDNISocios.Text = socio.DNI;
                txtTlfSocios.Text = socio.Telefono;
                txtEmailSocios.Text = socio.Email;
                txtCuantiaMensual.Text = Convert.ToString(socio.CuantiaMensual);
                btnAceptarAdd.Visibility = Visibility.Hidden;
            }

            txtNombreSocios.IsReadOnly = true;
            txtDNISocios.IsReadOnly = true;
            txtTlfSocios.IsReadOnly = true;
            txtEmailSocios.IsReadOnly = true;
            txtCuantiaMensual.IsReadOnly = true;

            txtNombreSocios.IsEnabled = true;
            txtDNISocios.IsEnabled = true;
            txtTlfSocios.IsEnabled = true;
            txtEmailSocios.IsEnabled = true;
            txtCuantiaMensual.IsEnabled = true;
            btnAnadirSocios.IsEnabled = true;
        }

        private void btnVolverMenu_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }

        private void socios_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Visibility = Visibility.Collapsed;
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
