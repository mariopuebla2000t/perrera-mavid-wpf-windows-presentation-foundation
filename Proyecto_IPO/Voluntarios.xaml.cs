﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;

namespace Proyecto_IPO
{
    /// <summary>
    /// Lógica de interacción para Voluntarios.xaml
    /// </summary>
    public partial class Voluntarios : Window
    {
        public List<Voluntario> listVoluntarios;
        public XmlDocument doc = new XmlDocument();

        public Voluntarios()
        {
            InitializeComponent();
            listVoluntarios = CargarXMLVoluntarios();
            lstVoluntarios.ItemsSource = listVoluntarios;
        }


        private List<Voluntario> CargarXMLVoluntarios()
        {
            List<Voluntario> listado = new List<Voluntario>();
            // Cargar contenido de prueba

            var fichero = Application.GetResourceStream(new Uri("XMLs/Voluntarios.xml", UriKind.Relative));
            doc.Load(fichero.Stream);
            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                var nuevoVoluntario = new Voluntario("", "", "", "", "", "");
                nuevoVoluntario.NombreCompleto = node.Attributes["NombreCompleto"].Value;
                nuevoVoluntario.DNI = node.Attributes["DNI"].Value;
                nuevoVoluntario.Telefono = node.Attributes["Telefono"].Value;
                nuevoVoluntario.Email = node.Attributes["Email"].Value;
                nuevoVoluntario.HorarioDispo = node.Attributes["HorarioDispo"].Value;
                nuevoVoluntario.ConcVet = node.Attributes["ConcVet"].Value; 
                listado.Add(nuevoVoluntario);
            }
            return listado;
        }

        private void btnElimVoluntarios_Click(object sender, RoutedEventArgs e)
        {
            Voluntario voluntario = lstVoluntarios.SelectedItem as Voluntario;
            if (voluntario != null)
            {
                string message = "Está seguro de que quiere eliminar de la lista a " + voluntario.NombreCompleto;
                MessageBoxResult result = MessageBox.Show(message, "¿Eliminar de la lista?", MessageBoxButton.YesNo, MessageBoxImage.Information);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        listVoluntarios.RemoveAt(lstVoluntarios.SelectedIndex);
                        lstVoluntarios.Items.Refresh();

                        XmlNode node = doc.SelectSingleNode("/Voluntarios/Voluntario[@DNI='" + voluntario.DNI + "']");
                        XmlNode parent = node.ParentNode;
                        parent.RemoveChild(node);
                        string newXML = doc.OuterXml;
                        string Path = Environment.CurrentDirectory.ToString();
                        string Parent = Directory.GetParent(Directory.GetParent(Path).FullName).FullName;
                        doc.Save(System.IO.Path.Combine(Parent, "XMLs/Voluntarios.xml"));
                        break;

                    case MessageBoxResult.No:
                        break;
                }

            }
            else
            {
                MessageBox.Show("Por favor, seleccione un sujeto.", "Seleccione un sujeto", MessageBoxButton.OK, MessageBoxImage.Warning);
                
            }

        }

        private void btnAnadirVoluntarios_Click(object sender, RoutedEventArgs e)
        {
            txtNombreVolunt.IsEnabled = true;
            txtDNIVolunt.IsEnabled = true;
            txtTlfVolunt.IsEnabled = true;
            txtEmailVolunt.IsEnabled = true;
            txtHorarioVolunt.IsEnabled = true;
            txtConcVetVolunt.IsEnabled = true;

            txtNombreVolunt.IsReadOnly = false;
            txtDNIVolunt.IsReadOnly = false;
            txtTlfVolunt.IsReadOnly = false;
            txtEmailVolunt.IsReadOnly = false;
            txtHorarioVolunt.IsReadOnly = false;
            txtConcVetVolunt.IsReadOnly = false;

            txtNombreVolunt.Text = "";
            txtDNIVolunt.Text = "";
            txtTlfVolunt.Text = "";
            txtEmailVolunt.Text = "";
            txtHorarioVolunt.Text = "";
            txtConcVetVolunt.Text = "";

            btnAceptarAdd.Content = "AÑADIR VOLUNTARIO";
            btnAceptarAdd.Visibility = Visibility.Visible;
            btnAnadirVolunt.IsEnabled = false;
        }

        private void btnEditarVoluntarios_Click(object sender, RoutedEventArgs e)
        {
            if (lstVoluntarios.SelectedItem != null)
            {
                Voluntario voluntario = (Voluntario)lstVoluntarios.SelectedItem;
                txtNombreVolunt.Text = voluntario.NombreCompleto;
                txtDNIVolunt.Text = voluntario.DNI;
                txtTlfVolunt.Text = voluntario.Telefono;
                txtEmailVolunt.Text = voluntario.Email;
                txtHorarioVolunt.Text = voluntario.HorarioDispo;
                txtConcVetVolunt.Text = voluntario.ConcVet;
                btnAceptarAdd.Visibility = Visibility.Hidden;

                txtNombreVolunt.IsEnabled = true;
                txtDNIVolunt.IsEnabled = true;
                txtTlfVolunt.IsEnabled = true;
                txtEmailVolunt.IsEnabled = true;
                txtHorarioVolunt.IsEnabled = true;
                txtConcVetVolunt.IsEnabled = true;
                btnAnadirVolunt.IsEnabled = false;

                txtNombreVolunt.IsReadOnly = false;
                txtDNIVolunt.IsReadOnly = false;
                txtTlfVolunt.IsReadOnly = false;
                txtEmailVolunt.IsReadOnly = false;
                txtHorarioVolunt.IsReadOnly = false;
                txtConcVetVolunt.IsReadOnly = false;

                btnAceptarAdd.Content = "EDITAR VOLUNTARIO";
                btnAceptarAdd.Visibility = Visibility.Visible;

            }
            else
            {
                MessageBox.Show("Por favor, seleccione un sujeto.", "Seleccione un sujeto", MessageBoxButton.OK, MessageBoxImage.Warning);
            }


        }


        private void btnAceptarAdd_Edit_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(txtNombreVolunt.Text) || String.IsNullOrEmpty(txtDNIVolunt.Text) ||
               String.IsNullOrEmpty(txtTlfVolunt.Text) || String.IsNullOrEmpty(txtEmailVolunt.Text) ||
               String.IsNullOrEmpty(txtHorarioVolunt.Text) || String.IsNullOrEmpty(txtConcVetVolunt.Text))
            {
                MessageBox.Show("Por favor, asegurese de que haya rellenado todos los campos correctamente.", "Rellene todos los campos", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                string Path = Environment.CurrentDirectory.ToString();
                string Parent = Directory.GetParent(Directory.GetParent(Path).FullName).FullName;

                XmlNode root = doc.DocumentElement;
                //Create a new attrtibute.  
                XmlElement elem = doc.CreateElement("Voluntario");
                XmlAttribute nombreCompleto = doc.CreateAttribute("NombreCompleto");
                XmlAttribute dni = doc.CreateAttribute("DNI");
                XmlAttribute telefono = doc.CreateAttribute("Telefono");
                XmlAttribute email = doc.CreateAttribute("Email");
                XmlAttribute horarioDispo = doc.CreateAttribute("HorarioDispo");
                XmlAttribute concVet = doc.CreateAttribute("ConcVet");

                if (!btnAceptarAdd.Content.Equals("AÑADIR VOLUNTARIO"))
                {
                    Voluntario voluntario = lstVoluntarios.SelectedItem as Voluntario;
                    listVoluntarios.RemoveAt(lstVoluntarios.SelectedIndex);
                    lstVoluntarios.Items.Refresh();

                    XmlNode node = doc.SelectSingleNode("/Voluntarios/Voluntario[@DNI='" + voluntario.DNI + "']");
                    XmlNode parent = node.ParentNode;
                    parent.RemoveChild(node);
                    string newXML = doc.OuterXml;
                }

                nombreCompleto.Value = txtNombreVolunt.Text;
                dni.Value = txtDNIVolunt.Text;
                telefono.Value = txtTlfVolunt.Text;
                email.Value = txtEmailVolunt.Text;
                horarioDispo.Value = txtHorarioVolunt.Text;
                concVet.Value = txtConcVetVolunt.Text;

                elem.Attributes.Append(nombreCompleto);
                elem.Attributes.Append(dni);
                elem.Attributes.Append(telefono);
                elem.Attributes.Append(email);
                elem.Attributes.Append(horarioDispo);
                elem.Attributes.Append(concVet);

                root.InsertAfter(elem, root.LastChild);
                doc.Save(System.IO.Path.Combine(Parent, "XMLs/Voluntarios.xml"));

                if (btnAceptarAdd.Content.Equals("AÑADIR VOLUNTARIOS"))
                {
                    MessageBox.Show("El voluntario con nombre " + txtNombreVolunt.Text + " ha sido añadido con éxito a la lista y al fichero XML.", "Voluntario añadido", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("El voluntario con nombre " + txtNombreVolunt.Text + " ha sido editado con éxito", "Voluntario editado", MessageBoxButton.OK, MessageBoxImage.Information);
                }

                txtNombreVolunt.IsEnabled = false;
                txtDNIVolunt.IsEnabled = false;
                txtTlfVolunt.IsEnabled = false;
                txtEmailVolunt.IsEnabled = false;
                txtHorarioVolunt.IsEnabled = false;
                txtConcVetVolunt.IsEnabled = false;
                btnAnadirVolunt.IsEnabled = true;

                listVoluntarios.Add(new Voluntario(txtNombreVolunt.Text, txtDNIVolunt.Text,
                    txtTlfVolunt.Text, txtEmailVolunt.Text, txtHorarioVolunt.Text, txtConcVetVolunt.Text));
                lstVoluntarios.Items.Refresh();
            }
        }


        private void lstVoluntarios_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lstVoluntarios.SelectedItem != null)
            {
                Voluntario voluntario = (Voluntario)lstVoluntarios.SelectedItem;
                txtNombreVolunt.Text = voluntario.NombreCompleto;
                txtDNIVolunt.Text = voluntario.DNI;
                txtTlfVolunt.Text = voluntario.Telefono;
                txtEmailVolunt.Text = voluntario.Email;
                txtHorarioVolunt.Text = voluntario.HorarioDispo;
                txtConcVetVolunt.Text = voluntario.ConcVet;
                btnAceptarAdd.Visibility = Visibility.Hidden;
            }

            txtNombreVolunt.IsReadOnly = true;
            txtDNIVolunt.IsReadOnly = true;
            txtTlfVolunt.IsReadOnly = true;
            txtEmailVolunt.IsReadOnly = true;
            txtHorarioVolunt.IsReadOnly = true;
            txtConcVetVolunt.IsReadOnly = true;

            txtNombreVolunt.IsEnabled = true;
            txtDNIVolunt.IsEnabled = true;
            txtTlfVolunt.IsEnabled = true;
            txtEmailVolunt.IsEnabled = true;
            txtHorarioVolunt.IsEnabled = true;
            txtConcVetVolunt.IsEnabled = true;
            btnAnadirVolunt.IsEnabled = true;
        }

        private void btnVolverMenu_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }

        private void voluntarios_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Visibility = Visibility.Collapsed;
        }
    }
}

    
    

