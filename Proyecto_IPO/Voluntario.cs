﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Media.Imaging;

public class Voluntario
{
	public string NombreCompleto { get; set; }
	public string DNI { get; set; }
	public string Telefono { get; set; }
	public string Email { get; set; }
	public string HorarioDispo  { get; set; }
	public string ConcVet { get; set; }

	//Faltaria las imagenes de la zona de disponibilidad y de su cara

	public Voluntario(string NombreCompleto, string DNI, string Telefono, string Email, string HorarioDispo, string ConcVet)
	{
		this.NombreCompleto = NombreCompleto;
		this.DNI = DNI;
		this.Telefono = Telefono;
		this.Email = Email;
		this.HorarioDispo = HorarioDispo;
		this.ConcVet = ConcVet;
	}

}