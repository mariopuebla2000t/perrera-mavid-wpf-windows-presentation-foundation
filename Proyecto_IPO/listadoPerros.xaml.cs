﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;
using Brushes = System.Windows.Media.Brushes;

namespace Proyecto_IPO
{
    /// <summary>
    /// Lógica de interacción para listadoPerros.xaml
    /// </summary>
    
    public partial class listadoPerros : Window
    {
        public List<Perro> listPerros;
        public XmlDocument doc = new XmlDocument();

        public listadoPerros()
        { 
            InitializeComponent();
            listPerros = CargarXMLPerros();
            lstPerros.ItemsSource = listPerros;

        }

        private List<Perro> CargarXMLPerros()
        {
            List<Perro> listado = new List<Perro>();
            // Cargar contenido de prueba

            var fichero = Application.GetResourceStream(new Uri("XMLs/Perros.xml", UriKind.Relative));
            doc.Load(fichero.Stream);
            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                var nuevoPerro = new Perro("", "", "", 0, "", "", null, "");
                nuevoPerro.Nombre = node.Attributes["Nombre"].Value;
                nuevoPerro.Sexo = node.Attributes["Sexo"].Value;
                nuevoPerro.Raza = node.Attributes["Raza"].Value;
                nuevoPerro.Edad = Convert.ToInt32(node.Attributes["Edad"].Value);
                nuevoPerro.Fecha = node.Attributes["Fecha"].Value;
                nuevoPerro.Descripcion = node.Attributes["Descripcion"].Value;
                nuevoPerro.Foto = new Uri(Environment.CurrentDirectory + node.Attributes["Foto"].Value);
                nuevoPerro.DNIApadrinador = node.Attributes["DNIApadrinador"].Value;
                listado.Add(nuevoPerro);
            }
            return listado;
        }


        void MouseDoubleClick_lstPerros(object sender, MouseEventArgs e)
        {
            if (lstPerros.SelectedItem != null)
            {
                Perro perro = (Perro)lstPerros.SelectedItem;
                infoPerros infoPerros = new infoPerros(this);
                infoPerros.txtNombre.Text = perro.Nombre;
                infoPerros.txtSexo.Text = perro.Sexo;
                infoPerros.txtRaza.Text = perro.Raza;
                infoPerros.txtEdad.Text = Convert.ToString(perro.Edad);
                infoPerros.txtFecha.Text = perro.Fecha;
                infoPerros.txtDescripcion.Text = perro.Descripcion;
                infoPerros.imgPerro.Source = new BitmapImage(perro.Foto);
                infoPerros.txtPath.Text = Convert.ToString(perro.Foto);

                infoPerros.txtNombre.IsReadOnly = true;
                infoPerros.txtSexo.IsReadOnly = true;
                infoPerros.txtRaza.IsReadOnly = true;
                infoPerros.txtEdad.IsReadOnly = true;
                infoPerros.txtFecha.IsReadOnly = true;
                infoPerros.txtDescripcion.IsReadOnly = true;

                infoPerros.txtNombre.IsEnabled = true;
                infoPerros.txtSexo.IsEnabled = true;
                infoPerros.txtRaza.IsEnabled = true;
                infoPerros.txtEdad.IsEnabled = true;
                infoPerros.txtFecha.IsEnabled = true;
                infoPerros.txtDescripcion.IsEnabled = true;

                if (perro.DNIApadrinador != "0")
                {
                    infoPerros.btnApadrinado.Content = "Apadrinado";
                    infoPerros.btnApadrinado.Foreground = Brushes.Green;
                }
                else
                {
                    infoPerros.btnApadrinado.Content = "No apadrinado";
                    infoPerros.btnApadrinado.Foreground = Brushes.Red;
                    infoPerros.btnApadrinado.IsEnabled = false;
                }
                infoPerros.Show();
            }
        }

        private void btnAniadir_Click(object sender, RoutedEventArgs e)
        {
            infoPerros AddPerro = new infoPerros(this);
            AddPerro.Owner = this;
            AddPerro.btnSubirImagen.Visibility = Visibility.Visible;
            AddPerro.btnAddPerro.Visibility = Visibility.Visible;
            AddPerro.txtDNIApadrinador.Visibility = Visibility.Visible;
            AddPerro.lblDNIApadrinador.Visibility = Visibility.Visible;
            AddPerro.btnAddPerro.IsEnabled = false;
            AddPerro.Show();

        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            Perro perro = lstPerros.SelectedItem as Perro;
            string message = "Está seguro de que quiere eliminar de la lista a " + perro.Nombre;
            MessageBoxResult result = MessageBox.Show(message, "¿Eliminar de la lista?", MessageBoxButton.YesNo, MessageBoxImage.Information);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    listPerros.RemoveAt(lstPerros.SelectedIndex);
                    lstPerros.Items.Refresh();

                    XmlNode node = doc.SelectSingleNode("/Perros/Perro[@Nombre='" + perro.Nombre + "']");
                    XmlNode parent = node.ParentNode;
                    parent.RemoveChild(node);
                    string newXML = doc.OuterXml;
                    string Path = Environment.CurrentDirectory.ToString();
                    string Parent = Directory.GetParent(Directory.GetParent(Path).FullName).FullName;
                    doc.Save(System.IO.Path.Combine(Parent, "XMLs/Perros.xml"));
                    break;

                case MessageBoxResult.No:
                    break;
            }
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {

            if (lstPerros.SelectedItem != null)
            {
                Perro perro = (Perro)lstPerros.SelectedItem;
                infoPerros infoPerros = new infoPerros(this);
                infoPerros.txtNombre.Text = perro.Nombre;
                infoPerros.txtSexo.Text = perro.Sexo;
                infoPerros.txtRaza.Text = perro.Raza;
                infoPerros.txtEdad.Text = Convert.ToString(perro.Edad);
                infoPerros.txtFecha.Text = perro.Fecha;
                infoPerros.txtDescripcion.Text = perro.Descripcion;
                infoPerros.txtDNIApadrinador.Text = perro.DNIApadrinador;
                infoPerros.imgPerro.Source = new BitmapImage(perro.Foto);
                infoPerros.txtPath.Text = Convert.ToString(perro.Foto);

                infoPerros.txtNombre.IsEnabled = true;
                infoPerros.txtSexo.IsEnabled = true;
                infoPerros.txtRaza.IsEnabled = true;
                infoPerros.txtEdad.IsEnabled = true;
                infoPerros.txtFecha.IsEnabled = true;
                infoPerros.txtDescripcion.IsEnabled = true;
                infoPerros.txtDNIApadrinador.IsEnabled = true;
                infoPerros.btnAddPerro.Content = "EDITAR PERRO";
                infoPerros.btnAddPerro.IsEnabled = true;
                infoPerros.btnApadrinado.Visibility = Visibility.Hidden;

                infoPerros.btnSubirImagen.Visibility = Visibility.Visible;
                infoPerros.btnAddPerro.Visibility = Visibility.Visible;
                infoPerros.txtDNIApadrinador.Visibility = Visibility.Visible;
                infoPerros.lblDNIApadrinador.Visibility = Visibility.Visible;
                infoPerros.btnAddPerro.IsEnabled = true;

                infoPerros.Show();
            }
        }

        private void btnVolverMenu_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }

        private void listadoperros_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Visibility = Visibility.Collapsed;
        }

    }


}