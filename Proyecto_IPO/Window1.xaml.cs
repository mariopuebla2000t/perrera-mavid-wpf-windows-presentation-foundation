﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Proyecto_IPO
{
    /// <summary>
    /// Lógica de interacción para Window1.xaml
    /// </summary>
   
    public partial class Window1 : Window
    {
        public listadoPerros perros = new listadoPerros();
        public Socios socios = new Socios();
        public Voluntarios voluntarios = new Voluntarios();

        public Window1()
        {
            InitializeComponent();
        }

        private void MouseDoubleClick_SalirApp(object sender, MouseButtonEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void MouseDoubleClick_GestionPerros(object sender, MouseButtonEventArgs e)
        {
            perros.Show();
            perros.Owner = this;
            perros.lblEstado.Content = lblMenuDatosUsu.Content;
        }

        private void MouseDoubleClick_GestionVoluntarios(object sender, MouseButtonEventArgs e)
        {
            voluntarios.Show();
            voluntarios.Owner = this;
            voluntarios.lblEstado.Content = lblMenuDatosUsu.Content;

        }

        private void MouseDoubleClick_GestionSocios(object sender, MouseButtonEventArgs e)
        {
            socios.Show();
            socios.Owner = this;
            socios.lblEstado.Content = lblMenuDatosUsu.Content;
        }

        private void VentanaMenuPrincipal_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
