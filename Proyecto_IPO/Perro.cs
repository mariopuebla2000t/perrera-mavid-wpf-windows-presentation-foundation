﻿using System;
using System.Drawing;
using System.Windows.Media.Imaging;

public class Perro
{
	public string Nombre { get; set; }
	public string Sexo { get; set; }
	public string Raza { get; set; }
	public int Edad { get; set; }
	public string Fecha { get; set; }
	public string Descripcion { get; set; }
	public Uri Foto { get; set; }
	public string DNIApadrinador { get; set; }

	public Perro(string Nombre, string Sexo, string Raza, int Edad, string Fecha, string Descripcion, Uri Foto, string DNIApadrinador)
	{
		this.Nombre = Nombre;
		this.Sexo = Sexo;
		this.Raza = Raza;
		this.Edad = Edad;
		this.Fecha = Fecha;
		this.Descripcion = Descripcion;
		this.Foto = Foto;
		this.DNIApadrinador = DNIApadrinador;
	}
}